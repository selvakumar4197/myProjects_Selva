module.exports = (app) => {
    const employee = require('../controllers/note.controller.js');

    // Create a new Note
    app.post('/employee', employee.create);

    // Retrieve all Notes
    app.get('/employee', employee.findAll);

    // Retrieve a single Note with noteId
    app.get('/employee/:noteId', employee.findOne);

    // Update a Note with noteId
    app.put('/employee/:noteId',  employee.update);

    // Delete a Note with noteId
    app.delete('/employee/:noteId', employee.delete);
}