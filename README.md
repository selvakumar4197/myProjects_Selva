PROJECT TITLE :Web Services to display employee details from mongodb

GETTING STARTED :Installation of MongoDB,Nodejs,npm framework like express ,mongoose etc.

DEFINING API FOR PERFORMING ALL OPERATIONS: Define API to getall,getbyid,deleteall,deletebyid from mongodb
Post required number of records in mongodb using post api using POSTMAN application

DISPLAY RECORDS USING BOOTSTRAP:
Call  getall api using ajax function
The returned format will be in JSON format
Append the content to each divisions
Search a particular record using filter options
Sort the employee details by their name
