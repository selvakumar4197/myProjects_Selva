const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    title: String,
    name:String,
    content: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Note', NoteSchema);